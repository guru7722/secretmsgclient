import "./styles/App.css";
import Navbar from "./components/Navbar";
import Login from "./routes/Login";
import Home from "./routes/Home";
import { Route, Routes } from "react-router-dom";
import About from "./routes/About";
import Help from "./routes/Help";
import Profile from "./routes/profile";
import Registration from "./routes/Registration";
import Footer from "./routes/Footer";
import Socialhandles from "./components/SocialHandles";
import axios from "axios";

const backendPath = 'http://localhost:8000/';

function App() {
  //login post requst
  function login(email, Password) {
    return new Promise(( resolve, reject)=>{
      axios
        .post(`${backendPath}login`, { email: email, password: Password })
        .then((response) =>  resolve(response.data))
        .catch((err) => reject(err.response.data));
    });
  }
  //registeration post req handling
    const register= (username,useremail,usernumber,userpassword)=>
    {
      return new Promise(( resolve,reject)=>{
         axios.post(`${backendPath}register`, { name:username, email:useremail,number:usernumber,password:userpassword })
        .then((response)=> resolve(response.data))
        .catch((err)=>reject(err.response.data))
      })
    }

   //auths 
 const authuser=()=>{
    return new Promise ((resolve,reject)=>{
      axios
      .get("http://localhost:8000/login")
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        reject(err.response.data);
      });
    })
  }
//logOut me 
  const logmeOut=()=>{
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:8000/logout')
    .then((response)=>resolve(response.data))
    .catch((err)=>reject(err.response.data))
    })
  }
  return (
    <div className="App">
      <Navbar auth={authuser} logOut={logmeOut}/>
      <Socialhandles />
      <Routes>
        <Route exact path="/" element={<Home auth={authuser}/>}></Route>
        <Route exact path="login" element={<Login loginUser={login}  auth={authuser}/>}></Route>
        <Route exact path="profile" element={<Profile auth={authuser} />}></Route>
        <Route exact path="about" element={<About />}></Route>
        <Route exact path="help" element={<Help />}></Route>
        <Route exact path="registration" element={<Registration userRegister={ register} auth={authuser}/>}></Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
