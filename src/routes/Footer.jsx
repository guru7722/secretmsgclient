import React from 'react';
import '../styles/Footer.css'
import logo from '../imgs/Component 7.png'

export default function Footer() {
  return (
    <>
        <div className='Footer-main'>
          <img src={logo} alt="Footer-logo" className='Logo'/>
          <div className='text-field'>
         <p>  Welcome to this place to see the magic of this app enjoy your day have fun!</p>
            <hr></hr>
          </div>
        </div>
    </>
  );
}
